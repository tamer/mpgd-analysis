#include <iostream>
#include <vector>
#include <math.h>

#include "Rechit.h"
#include "Cluster.h"
#include "Geometry.h"



// Rechit::Rechit(int chamber, int direction, Cluster cluster) {
//     fChamber = chamber;
//     fCenter = mappingStart[chamber][direction] + mappingScale[chamber][direction]*cluster.getCenter()*mappingPitch[chamber] - mappingCorrections[chamber][direction];
//     fClusterSize = cluster.getSize();
//     fError = fClusterSize*mappingPitchSqrt12[chamber];
// }

// Rechit::Rechit(int chamber, double center, int clusterSize) {
//     fChamber = chamber;
//     fCenter = center;
//     fClusterSize = clusterSize;
//     fError = fClusterSize*mappingPitchSqrt12[chamber];
// }

Rechit::Rechit(int chamber, double x, double y, double errorX, double errorY, int clusterSize) {
    m_chamber = chamber;
    m_x = x;
    m_y = y;
    m_errorX = errorX;
    m_errorY = errorY;
    m_clusterSize = clusterSize;
}

void Rechit::setGlobalPosition(double x, double y, double z, double errorX, double errorY) {
    m_globalX = x;
    m_globalY = y;
    m_globalZ = z;
    m_errorGlobalX = errorX;
    m_errorGlobalY = errorY;
}


