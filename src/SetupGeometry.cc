#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <stdexcept>

#include "SetupGeometry.h"
#include "DataFrame.h"
#include "tinyxml2.h"

SetupGeometry::SetupGeometry(std::string geometryFile) {

    tinyxml2::XMLDocument setupGeometryDoc;
    if (setupGeometryDoc.LoadFile(geometryFile.c_str())) {
        throw std::invalid_argument("Could not open setup geometry file " + geometryFile + ". Does the file exist?");
    }

    tinyxml2::XMLElement *setupElement = setupGeometryDoc.FirstChildElement("setup");

    const char *setupLabel;
    setupElement->QueryStringAttribute("label", &setupLabel);
    std::cout << setupLabel << std::endl;

    DetectorGeometry *detector;
    tinyxml2::XMLElement *detectorElement = setupElement->FirstChildElement("detector");
    while (detectorElement) {
        const char *chamberLabel;
        int detectorID;
        bool isTracker;
        double baseSmall, baseLarge, height;
        int nEta, nStrips;
        double x, y, z, phi_z;

        detectorElement->QueryStringAttribute("label", &chamberLabel);
        detectorElement->QueryBoolAttribute("tracker", &isTracker);
        detectorElement->QueryIntAttribute("id", &detectorID);

        const char *geometryName = detectorElement->FirstChildElement("geometry")->GetText();
        // Read detector geometry from corresponding file:
        std::string detectorGeometryPath = "geometry/detectors/"+std::string(geometryName)+".xml";
        tinyxml2::XMLDocument detectorGeometryDoc;
        if (detectorGeometryDoc.LoadFile(detectorGeometryPath.c_str())) {
            throw std::invalid_argument("Could not open detector geometry file " + detectorGeometryPath + ". Does the file exist?");
        }
        tinyxml2::XMLElement *geometryElement = detectorGeometryDoc.FirstChildElement("detectorGeometry")->FirstChildElement("geometry");
        geometryElement->FirstChildElement("baseSmall")->QueryDoubleText(&baseSmall);
        geometryElement->FirstChildElement("baseLarge")->QueryDoubleText(&baseLarge);
        geometryElement->FirstChildElement("height")->QueryDoubleText(&height);

        tinyxml2::XMLElement *readoutElement = detectorGeometryDoc.FirstChildElement("detectorGeometry")->FirstChildElement("readout");
        readoutElement->FirstChildElement("nEta")->QueryIntText(&nEta);
        readoutElement->FirstChildElement("nStrips")->QueryIntText(&nStrips);

        // Read alignment from detector geometry file:
        tinyxml2::XMLElement *alignmentElement = detectorElement->FirstChildElement("alignment");
        alignmentElement->FirstChildElement("x")->QueryDoubleText(&x);
        alignmentElement->FirstChildElement("y")->QueryDoubleText(&y);
        alignmentElement->FirstChildElement("z")->QueryDoubleText(&z);
        alignmentElement->FirstChildElement("phi_z")->QueryDoubleText(&phi_z);

        DetectorGeometry detector(
            detectorID, baseSmall, baseLarge, height, nEta, nStrips
        );
        detector.setPosition(
            x, y, z, phi_z
        );
        detectorMap[detector.getChamber()] = detector;
        if (isTracker) trackerChambers.push_back(detector.getChamber());
        detectorMap[detector.getChamber()].print();

        // Go on to next detector
        detectorElement = detectorElement->NextSiblingElement("detector");
    }
}

bool SetupGeometry::isTracking(int chamber) {
    return std::count(trackerChambers.begin(), trackerChambers.end(), chamber)>0;
}

DetectorGeometry *SetupGeometry::getChamber(int chamber) {
    if (detectorMap.count(chamber)>0) {
        return &detectorMap.at(chamber);
    } else {
        throw std::invalid_argument(std::string("The detector number " + std::to_string(chamber) + " does not exist or has no mapping."));
    }
}

void SetupGeometry::print() {
    geometryDataFrame.print();
}
