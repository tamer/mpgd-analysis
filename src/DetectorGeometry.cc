#include <math.h>

#include "Cluster.h"
#include "Rechit.h"
#include "DetectorGeometry.h"

DetectorGeometry::DetectorGeometry(const DetectorGeometry& detector) {
    DetectorGeometry(
            detector.getChamber(),
            detector.getBaseNarrow(), detector.getBaseWide(),
            detector.getHeight(), detector.getNEta(), detector.getNStrips()
            );
    setPosition(detector.getPositionX(), detector.getPositionY(), detector.getPositionZ(), detector.getTheta());
}

DetectorGeometry::DetectorGeometry(int chamber, double baseNarrow, double baseWide, double height, int nEta, int nStrips) {
    m_chamber = chamber;
    m_baseNarrow = baseNarrow;
    m_baseWide = baseWide;
    m_height = height;
    m_numberPartitions = nEta;
    m_numberStrips = nStrips;
    m_etaHeight = height/nEta;

    m_partitionYs.reserve(m_numberPartitions);
    m_partitionYTops.reserve(m_numberPartitions);
    m_partitionWidths.reserve(m_numberPartitions);
    m_partitionStripPitches.reserve(m_numberPartitions);
    m_partitionStripPitchesSqrt12.reserve(m_numberPartitions);
    for (int eta=0; eta<m_numberPartitions; eta++) {
        m_partitionYs.push_back( -0.5*m_height + m_etaHeight*(0.5 + (double)(m_numberPartitions-eta-1)));
        m_partitionYTops.push_back(m_partitionYs[eta] + 0.5*m_etaHeight);
        m_partitionWidths.push_back(m_baseNarrow + m_partitionYs[eta]*(m_baseWide-m_baseNarrow)/m_height);
        m_partitionStripPitches.push_back(m_partitionWidths[eta] / m_numberStrips);
        m_partitionStripPitchesSqrt12.push_back(m_partitionStripPitches[eta] * INVERSE_SQRT_12);
    }

    // Calculate detector geometric parameters:
    m_originY = - baseNarrow*height/(baseWide-baseNarrow) - 0.5*m_height;
    m_area = 0.5*(baseWide+baseNarrow)*height;
    m_aperture = 2*atan(0.5*baseWide/abs(m_originY-0.5*m_height));
}

DetectorGeometry::DetectorGeometry(int chamber, double baseNarrow, double baseWide, double height, int nEta, int nStrips, double x, double y, double z, double theta)
:DetectorGeometry(chamber, baseNarrow, baseWide, height, nEta, nStrips) {
    setPosition(x, y, z, theta);
}

void DetectorGeometry::print() const {
    std::cout << "-----------------------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "Detector " << m_chamber << std::endl;
    std::cout << "Narrow base " << m_baseNarrow << ", wide base " << m_baseWide << ", height " << m_height << std::endl;
    std::cout << "Eta partitions " << m_numberPartitions << ", strips " << m_numberStrips << ", eta partition height " << m_etaHeight << std::endl;

    for (int eta=0; eta<m_numberPartitions; eta++) {
        std::cout << "    eta partition " << eta+1;
        std::cout << ", middle y " << m_partitionYs[eta] << ", width " << m_partitionWidths[eta];
        std::cout << ", strip pitch " << m_partitionStripPitches[eta];
        std::cout << ", expected resolution " << m_partitionStripPitchesSqrt12[eta] << std::endl;
    }

    // Print detector geometric parameters:
    std::cout << "Origin " << m_originY;
    std::cout << ", area " << m_area;
    std::cout << ", aperture " << m_aperture;
    std::cout << ", pitch " << m_aperture/m_numberStrips;
    std::cout << ", expected resolution " << m_aperture/m_numberStrips/pow(12, 0.5) << std::endl;

    std::cout << "Position (";
    std::cout << m_position[0] << ", " << m_position[1] << ", " << m_position[2];
    std::cout << "), angle ";
    std::cout << m_theta << std::endl;
}

double DetectorGeometry::getY(int eta) const {
    return m_partitionYs[eta-1];
}

double DetectorGeometry::getYTop(int eta) const {
    return m_partitionYTops[eta-1];
}

double DetectorGeometry::getWidth(int eta) const {
    return m_partitionWidths[eta-1];
}

double DetectorGeometry::getStripPitch(int eta) const {
    return m_partitionStripPitches[eta-1];
}

double DetectorGeometry::getStripPitchSqrt12(int eta) const {
    return m_partitionStripPitchesSqrt12[eta-1];
}

Rechit DetectorGeometry::createRechit(Cluster cluster) const {
    double x = -0.5*getWidth(cluster.getEta()) + getStripPitch(cluster.getEta()) * cluster.getCenter(); // geometrical center of cluster as x
    double y = getY(cluster.getEta()); // center of eta partition as y
    Rechit rechit(
            m_chamber, x, y,
            cluster.getSize() * getStripPitchSqrt12(cluster.getEta()), // uncertainty on x from cluster size
            m_etaHeight * 0.5, // uncertainty on y from eta partition height. TODO: support different eta heights
            cluster.getSize()
            );
    rechit.setR(sqrt(pow(x,2) + pow(y-getOriginY(),2)));
    rechit.setPhi(atan(x/(y-getOriginY())));
    return rechit;
}

void DetectorGeometry::mapRechit(Rechit *rechit) {
    double localX = rechit->getX();
    double localY = rechit->getY();
    double c = cos(m_theta);
    double s = sin(m_theta);
    double errorX = rechit->getErrorX();
    double errorY = rechit->getErrorY();
    rechit->setR(sqrt(pow(localX,2) + pow(localY-getOriginY(),2)));
    rechit->setPhi(atan(localX/(localY-getOriginY())));
    rechit->setGlobalPosition(
            m_position[0] + localX*c - localY*s,
            m_position[1] + localX*s + localY*c,
            m_position[2],
            sqrt(pow(c*errorX,2) + pow(s*errorY,2)),
            sqrt(pow(s*errorX,2) + pow(c*errorY,2))
            );
}
