#include <cstdio>
#include <vector>

#include "DataFrame.h"
#include "DetectorGeometry.h"

class SetupGeometry {

    public:

        SetupGeometry(std::string geometryFile);

        void print();
        DetectorGeometry *getChamber(int chamber);
        bool isTracking(int chamber);

        /* List of tracking chamber IDs */
        std::vector<int> trackerChambers;
        /* Map from chamber number to detector */
        std::map<int, DetectorGeometry> detectorMap;

    private:

        DataFrame geometryDataFrame;
};
