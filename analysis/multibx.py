import os, sys, pathlib
import argparse
import logging
from tqdm import tqdm

import uproot
import numpy as np
import pandas as pd
import awkward as ak

from scipy.optimize import curve_fit
from scipy.stats import binned_statistic
from scipy.stats import norm

import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
import mplhep as hep

plt.style.use(hep.style.ROOT)
plt.rcParams.update({"font.size": 32})
plt.rcParams.update({"image.cmap": "Purples"})
logging.getLogger('matplotlib.font_manager').disabled = True
hep.cms.label()#, data=<True|False>, lumi=50, year=2017)

gauss_pdf = lambda x, k , mu, s: k * norm.pdf(x, mu, s)
gauss2_pdf = lambda x, k1, m1, s1, k2, m2, s2: gauss_pdf(x, m1, k1, s1) + gauss_pdf(x, m2, k2, s2)

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("ifile", type=pathlib.Path)
    parser.add_argument("odir", type=pathlib.Path)
    parser.add_argument("--fit", nargs="+", type=int, help="Range to fit gaussian peak")
    parser.add_argument("--range", nargs="+", type=int, help="Range to determine optimal latency")
    parser.add_argument("-m", "--multiplicities", action="store_true")
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("-n", "--events", type=int, default=-1)
    args = parser.parse_args()

    os.makedirs(args.odir, exist_ok=True)
    if args.verbose: logging.basicConfig(level=logging.DEBUG, format="%(message)s")
    else: logging.basicConfig(level=logging.INFO, format="%(message)s")

    with uproot.open(args.ifile) as digi_file:
        digi_tree = digi_file["outputtree"]
        if args.verbose: digi_tree.show()

        logging.info("Reading tree...")
        events_df = digi_tree.arrays(
            [
            "digiBX",
            "slot", "OH", "VFAT", "CH",
            "digiChamber", "digiEta", "digiStrip",
            ], entry_stop=args.events, library="pd"
        )

        def analyze_oh(oh_df):

            def analyze_vfat(vfat_df):

                vfat_df = vfat_df.reset_index()
                vfat = vfat_df["VFAT"].unique()[0]

                times = vfat_df["digiBX"]
                time_range = [times.min(), times.max()]

                ax = oh_axs.flat[oh_vfats_indices[vfat]]
                """ Plot total multiplicities per latency and determine optimal latency """
                logging.debug("All hits:")
                logging.debug(vfat_df)
                time_counts, time_bins, _ = ax.hist(
                    vfat_df["digiBX"],
                    bins=time_range[1]-time_range[0]+1,
                    range=(time_range[0]-0.5, time_range[1]+0.5),
                    histtype="step", linewidth=2, color="blue", label="Hits"
                )
                time_centers = 0.5*(time_bins[1:]+time_bins[:-1])

                """ Plot event counts per latency """
                vfat_df_unique = vfat_df.drop_duplicates("entry")
                logging.debug("One hit per event:")
                logging.debug(vfat_df_unique)
                ax.hist(
                    vfat_df_unique["digiBX"],
                    bins=time_range[1]-time_range[0]+1,
                    range=(time_range[0]-0.5, time_range[1]+0.5),
                    histtype="step", linewidth=2, color="green", label="Events"
                )
                ax.set_xlim(0, time_range[1])
                ax.legend(loc="upper right")
                ax.set_title("VFAT {}".format(vfat))
                ax.set_xlabel("Hit time (BX)")
                ax.set_ylabel("Number of events")

                """ Fit with gaussian """
                if args.fit:
                    fit_range = np.array(args.fit).astype(float)
                    latency_filter = (time_centers>time_range[0])&(time_centers<time_range[1])
                    popt = [ np.max(time_counts), 0.5*np.sum(fit_range), np.diff(fit_range)[0] ]
                    try:
                        popt, pcov = curve_fit(gauss_pdf, time_centers[latency_filter], time_counts[latency_filter], p0=popt)
                        perr = np.sqrt(np.diag(pcov))
                        x = np.linspace(*fit_range, 100)
                        ax.plot(x, gauss_pdf(x, *popt), "-", color="red")
                        logging.debug(popt)
                        logging.debug(perr)
                        ax.annotate(
                            "Mean {:1.1f} $\pm$ {:1.1f}\nSigma {:1.1f} $\pm$ {:1.1f}".format(popt[1], perr[1], popt[2], perr[2]),
                            (0.55, 0.7), xycoords="axes fraction", va="top"
                        )
                    except TypeError:
                        logging.info("Fit failed, skipping")
                    optimal_latency = popt[1]
                elif args.range:
                    latency_filter = (time_centers>args.range[0])&(time_centers<args.range[1])
                    ls, cs = time_centers[latency_filter], time_counts[latency_filter]
                    optimal_latency = (np.sum(ls*cs)/np.sum(cs)).astype(int)
                    optimal_latency = time_centers[np.argmax(time_counts)].astype(int)
                    logging.info("Analyzed slot {}, OH {}, VFAT {}: latency {}".format(slot, oh, vfat, optimal_latency))

                    ax.annotate(
                        "Optimal latency {}".format(optimal_latency),
                        (0.05,0.7), xycoords="axes fraction"
                    )
                else:
                    pass
                
            # Analyze data for each VFAT:
            slot, oh = oh_df["slot"].unique()[0], oh_df["OH"].unique()[0]
            logging.debug(oh_df)

            oh_vfats = oh_df["VFAT"].unique()
            oh_vfats_indices = { vfat:i for i,vfat in enumerate(sorted(oh_vfats)) }
            nrows = 3
            ncols = int(len(oh_vfats)/nrows)
            oh_fig, oh_axs = plt.subplots(ncols=ncols, nrows=nrows, figsize=(11*ncols, 9*nrows))

            oh_df.groupby("VFAT", as_index=False, group_keys=False).apply(analyze_vfat)

            oh_fig.tight_layout()
            oh_fig.savefig(args.odir / "slot{}_oh{}.png".format(slot, oh))

        # Analyze data for each OH:
        events_df.groupby(["slot", "OH"], as_index=False, group_keys=False).apply(analyze_oh)


        """ Analyze data for all chambers """
        events_ak = digi_tree.arrays(
            [
            "digiBX", "digiChamber", "VFAT",
            ], entry_stop=args.events
        )
        list_chambers = np.unique(ak.flatten(events_ak["digiChamber"]))
        n_chambers = len(list_chambers)
        timing_fig, timing_axs = plt.subplots(ncols=n_chambers, nrows=1, figsize=(11*n_chambers, 9))
        resolution_fig, resolution_ax = plt.subplots(figsize=(11, 9))
        time_range = [ak.min(events_ak["digiBX"]), ak.max(events_ak["digiBX"])]

        resolutions = list()
        err_resolutions = list()

        for i_chamber,max_chamber in enumerate(list_chambers):

            logging.info("Timing with {} chambers...".format(max_chamber))
            chamber_filter = events_ak["digiChamber"]<=max_chamber

            """ Keep only events in which the same VFAT has fired in all chambers: """
            vfat_filter = ak.std(events_ak["VFAT"], axis=1)==0
            chamber_filter = chamber_filter&vfat_filter

            time_filtered, chamber_filtered = events_ak["digiBX"][chamber_filter], events_ak["digiChamber"][chamber_filter]

            time_first = ak.mean(time_filtered, axis=1)
            time_weight = ak.count(time_filtered, axis=1)
            logging.debug(time_first)
            time_counts, time_bins, _ = timing_axs[i_chamber].hist(time_first, 
                bins=time_range[1]-time_range[0]+1,
                range=(time_range[0]-0.5, time_range[1]+0.5),
                #weights=time_weight,
                histtype="step", linewidth=2, label="{} chambers".format(max_chamber+1)
            )

            time_centers = 0.5*(time_bins[1:] + time_bins[:-1])
            popt = [ np.max(time_counts), 0.5*(time_range[-1]+time_range[0]), 0.5*(time_range[-1]+time_range[0]) ]
            popt, pcov = curve_fit(gauss_pdf, time_centers, time_counts, p0=popt)
            perr = np.sqrt(np.diag(pcov))
            x = np.linspace(*time_range, 1000)
            timing_axs[i_chamber].plot(x, gauss_pdf(x, *popt), "-", color="red")
            timing_axs[i_chamber].set_title("{} chambers".format(max_chamber+1))
            timing_axs[i_chamber].annotate(
                "Mean {:1.2f} $\pm$ {:1.2f}\nSigma {:1.2f} $\pm$ {:1.2f}".format(popt[1], perr[1], popt[2], perr[2]),
                (0.55, 0.9), xycoords="axes fraction", va="top"
            )
            resolutions.append(popt[2])
            err_resolutions.append(perr[2])

        resolution_ax.errorbar(np.arange(1, n_chambers+1), resolutions, yerr=err_resolutions, fmt="o--", color="blue")
        resolution_ax.set_xlabel("Number of layers")
        resolution_ax.set_ylabel("Time resolution (BX)")
        resolution_fig.savefig(args.odir / "resolution.png")
        timing_fig.tight_layout()
        timing_fig.savefig(args.odir / "timing.png")

if __name__=='__main__': main()
