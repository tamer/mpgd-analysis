import * as THREE from 'three';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';
import { TextGeometry } from 'three/addons/geometries/TextGeometry.js';

const renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 100000 );
const controls = new OrbitControls( camera, renderer.domElement );

var detectorThickness = 10;

var detectorZs = [];

$.getJSON("/geometry/"+geometry_name, function(setupGeometry) {
    console.log(setupGeometry);
    //const geometry = new THREE.CylinderGeometry(0.4 / Math.SQRT2, 1 / Math.SQRT2, 2);

    for (let detectorIndex in setupGeometry) {
        var detectorGeometry = setupGeometry[detectorIndex];
        console.log(detectorGeometry);
        var detectorZ = detectorGeometry["z"]*5;
        var detectorHeight = detectorGeometry["height"];
        var baseLarge = detectorGeometry["baseLarge"];
        var baseSmall = detectorGeometry["baseSmall"];
        console.log(detectorThickness, detectorZ, detectorHeight, baseLarge, baseSmall);
        detectorZs.push(detectorZ);

        const shape = new THREE.Shape();
        shape.moveTo( -0.5*baseSmall, -0.5*detectorHeight );
        shape.lineTo( +0.5*baseSmall, -0.5*detectorHeight );
        shape.lineTo( +0.5*baseLarge, +0.5*detectorHeight );
        shape.lineTo( -0.5*baseLarge, +0.5*detectorHeight );
        shape.lineTo( -0.5*baseSmall, -0.5*detectorHeight );

        const extrudeSettings = {
            steps: 2,
            depth: detectorThickness,
            bevelEnabled: true,
            bevelThickness: 1,
            bevelSize: 1,
            bevelOffset: 0,
            bevelSegments: 1
        };

        const geometry = new THREE.ExtrudeGeometry( shape, extrudeSettings );
        const material = new THREE.MeshBasicMaterial( {
            color: new THREE.Color("skyblue"),
            transparent: true,
            opacity: 0.6
        });
        const detector = new THREE.Mesh( geometry, material );
        detector.opacity = 0.5;

        var detectorEdgesGeometry = new THREE.EdgesGeometry( detector.geometry );
        var mat = new THREE.LineBasicMaterial( { color: 0x000000 } );
        var wireframe = new THREE.LineSegments( detectorEdgesGeometry, mat );
        detector.add( wireframe );

        detector.translateZ(detectorZ);
        scene.add( detector );
    }

    const textGeometry = new TextGeometry( "For informations ask antonello.pellecchia@cern.ch", {
        size: 80,
        height: 5,
        curveSegments: 12,
        bevelEnabled: true,
        bevelThickness: 10,
        bevelSize: 8,
        bevelOffset: 0,
        bevelSegments: 5
    });
    const textMaterial = new THREE.MeshBasicMaterial( {
        color: new THREE.Color("skyblue"),
        transparent: true,
        opacity: 0.6
    });
    const textObject = new THREE.Mesh( textGeometry, textMaterial );
    scene.add(textObject);

    camera.position.z = -3000;
    camera.rotation.z -= Math.PI/2;

    function animate() {
        requestAnimationFrame( animate );
        //detector.rotation.x += 0.01;
        //detector.rotation.y += 0.01;

        controls.update();

        renderer.render( scene, camera );
    }
    animate();
});

$.getJSON("/get/"+run_number+"/"+event_number, function(trackData) {
    console.log(trackData);
    
    // Draw track line:
    const material = new THREE.LineBasicMaterial({color: new THREE.Color( 'Crimson' )});

    function track(z) {
        return [
            trackData["trackInterceptX"] + trackData["trackSlopeX"]*z,
            trackData["trackInterceptY"] + trackData["trackSlopeY"]*z
        ];
    };

    const points = [];
    var z1 = -1000;
    var z2 = 1000;
    points.push( new THREE.Vector3( track(z1)[0], track(z1)[1], z1));
    points.push( new THREE.Vector3( track(z2)[0], track(z2)[1], z2));
    const trackGeometry = new THREE.BufferGeometry().setFromPoints( points );
    const line = new THREE.Line( trackGeometry, material );
    scene.add( line );

    // Draw strips with hit:
    var stripHeight = 100;
    var stripWidth = 5;
    for (let rechitIndex in trackData["rechitChamber"]) {
        var stripX = trackData["rechitGlobalX"][rechitIndex];
        var stripY = trackData["rechitGlobalY"][rechitIndex];

        console.log( stripX-0.5*stripWidth, stripY-0.5*stripHeight );
        const stripShape = new THREE.Shape();
        stripShape.moveTo( stripX-0.5*stripWidth, stripY-0.5*stripHeight );
        stripShape.lineTo( stripX-0.5*stripWidth, stripY+0.5*stripHeight);
        stripShape.lineTo( stripX+0.5*stripWidth, stripY+0.5*stripHeight);
        stripShape.lineTo( stripX+0.5*stripWidth, stripY-0.5*stripHeight);
        stripShape.lineTo( stripX-0.5*stripWidth, stripY-0.5*stripHeight);

        const extrudeSettings = {
            steps: 2,
            depth: detectorThickness,
            bevelEnabled: true,
            bevelThickness: 1,
            bevelSize: 1,
            bevelOffset: 0,
            bevelSegments: 1
        };

        const stripGeometry = new THREE.ExtrudeGeometry( stripShape, extrudeSettings );
        const stripMaterial = new THREE.MeshBasicMaterial( { color: new THREE.Color("Cornsilk")} );
        const stripObject = new THREE.Mesh( stripGeometry, stripMaterial );
        stripObject.translateZ(detectorZs[rechitIndex]);
        scene.add(stripObject);
    }
});


