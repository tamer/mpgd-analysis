#include <cstdio>
#include <iostream>
#include <fstream>
#include <cstdint>
#include <vector>
#include <array>
#include <bitset>
#include <signal.h>

#include <TFile.h>
#include <TTree.h>

#include "Digi.h"
#include "Cluster.h"
#include "DetectorGeometry.h"
#include "SetupGeometry.h"

#include "progressbar.h"

bool isInterrupted = false;
void interruptHandler(int dummy) {
    isInterrupted = true;
}

int main (int argc, char** argv) {

    if (argc<3) {
        std::cout << "Usage: DigiToRechits ifile ofile [--verbose] [--events n] [--geometry geometry_name]" << std::endl;
        return 0;
    }
    std::string ifile   = argv[1];
    std::string ofile   = argv[2];

    int max_events = -1;
    bool verbose = false;
    std::string geometry;
    for (int iarg=0; iarg<argc; iarg++) {
        std::string arg = argv[iarg];
        if (arg=="--verbose") verbose = true;
        else if (arg=="--events") max_events = atoi(argv[iarg+1]); 
        else if (arg=="--geometry") geometry = argv[iarg+1]; 
    }

    if (geometry == "") {
        std::cout << "Please specify a setup geometry" << std::endl;
        return -1;
    }

    if (max_events>=0) std::cout << "Analyzing " << max_events << " events" << std::endl;
    else std::cout << "Analyzing all events" << std::endl; 

    TFile digiFile(ifile.c_str(), "READ");
    TFile rechitFile(ofile.c_str(), "RECREATE", "Rechit tree file");

    TTree *digiTree = (TTree *) digiFile.Get("outputtree");
    TTree rechitTree("rechitTree","rechitTree");

    /* Define detector geometries */
    SetupGeometry setupGeometry("geometry/setups/"+geometry+".xml");

    // digi variables
    int orbitNumber, bunchCounter, eventCounter;
    std::vector<int> *vecDigiChamber = new std::vector<int>();
    std::vector<int> *vecDigiEta = new std::vector<int>();
    std::vector<int> *vecDigiDirection = new std::vector<int>();
    std::vector<int> *vecDigiStrip = new std::vector<int>();
    std::vector<int> *vecRawChannel = new std::vector<int>();

    // cluster variables
    int nclusters;
    std::vector<int> vecClusterChamber;
    std::vector<int> vecClusterEta;
    std::vector<int> vecClusterCenter;
    std::vector<int> vecClusterFirst;
    std::vector<int> vecClusterSize;

    // rechits variables
    std::vector<int> vecRechitChamber;
    std::vector<int> vecRechitEta;
    std::vector<double> vecRechitX;
    std::vector<double> vecRechitY;
    std::vector<double> vecRechitErrorX;
    std::vector<double> vecRechitErrorY;
    std::vector<double> vecRechitR;
    std::vector<double> vecRechitPhi;
    std::vector<double> vecRechitClusterSize;

    // support variables
    Rechit rechit;

    std::vector<Digi> digisInEvent;
    std::vector<Cluster> clustersInEvent;

    // digi variable branches
    digiTree->SetBranchAddress("orbitNumber", &orbitNumber);
    digiTree->SetBranchAddress("bunchCounter", &bunchCounter);
    digiTree->SetBranchAddress("eventCounter", &eventCounter);
    digiTree->SetBranchAddress("digiChamber", &vecDigiChamber);
    digiTree->SetBranchAddress("digiEta", &vecDigiEta);
    digiTree->SetBranchAddress("digiStrip", &vecDigiStrip);
    digiTree->SetBranchAddress("CH", &vecRawChannel);
    //digiTree->SetBranchAddress("digiDirection", &vecDigiDirection);

    // event branches
    rechitTree.Branch("orbitNumber", &orbitNumber);
    rechitTree.Branch("eventCounter", &eventCounter);
    rechitTree.Branch("bunchCounter", &bunchCounter);

    // cluster branches
    rechitTree.Branch("nclusters", &nclusters, "nclusters/I");
    rechitTree.Branch("clusterChamber", &vecClusterChamber);
    rechitTree.Branch("clusterEta", &vecClusterEta);
    rechitTree.Branch("clusterCenter", &vecClusterCenter);
    rechitTree.Branch("clusterFirst", &vecClusterFirst);
    rechitTree.Branch("clusterSize", &vecClusterSize);

    // rechit branches
    rechitTree.Branch("rawChannel", vecRawChannel);
    rechitTree.Branch("digiStrip", vecDigiStrip);
    rechitTree.Branch("rechitChamber", &vecRechitChamber);
    rechitTree.Branch("rechitEta", &vecRechitEta);
    rechitTree.Branch("rechitX", &vecRechitX);
    rechitTree.Branch("rechitY", &vecRechitY);
    rechitTree.Branch("rechitErrorX", &vecRechitErrorX);
    rechitTree.Branch("rechitErrorY", &vecRechitErrorY);
    rechitTree.Branch("rechitR", &vecRechitR);
    rechitTree.Branch("rechitPhi", &vecRechitPhi);
    rechitTree.Branch("rechitClusterSize", &vecRechitClusterSize);

    int nentries = digiTree->GetEntries();
    std::cout << nentries << " total events" <<  std::endl;
    if (max_events>=0) {
        std::cout << "Processing " << nentries << " events" << std::endl;
        nentries = max_events;
    }
    progressbar bar(nentries);

    signal(SIGINT, interruptHandler);
    for (int nevt=0; (!isInterrupted) && digiTree->LoadTree(nevt)>=0; ++nevt) {
        if ((max_events>=0) && (nevt>=max_events)) break;

        if (verbose) std::cout << "Event " << nevt << "/" << nentries << std::endl;
        else bar.update();
        //if ( nevt%1000==0 ) std::cout << "Unpacking event " << nevt << "\t\t\t\r";

        digiTree->GetEntry(nevt);

        vecClusterChamber.clear();
        vecClusterEta.clear();
        vecClusterCenter.clear();
        vecClusterFirst.clear();
        vecClusterSize.clear();

        vecRechitChamber.clear();
        vecRechitEta.clear();
        vecRechitX.clear();
        vecRechitY.clear();
        vecRechitErrorX.clear();
        vecRechitErrorY.clear();
        vecRechitR.clear();
        vecRechitPhi.clear();
        vecRechitClusterSize.clear();

        digisInEvent.clear();
        for (int ihit=0; ihit<vecDigiChamber->size(); ihit++)
            digisInEvent.push_back(Digi(
                        vecDigiChamber->at(ihit),
                        vecDigiEta->at(ihit),
                        vecDigiStrip->at(ihit)
                        ));
        clustersInEvent = Cluster::fromDigis(digisInEvent);

        /* Assign all clusters to detectors and create rechits */
        nclusters = clustersInEvent.size();
        for (int icluster=0; icluster<nclusters; icluster++) {

            int chamber = clustersInEvent[icluster].getChamber();
            vecClusterChamber.push_back(clustersInEvent[icluster].getChamber());
            vecClusterEta.push_back(clustersInEvent[icluster].getEta());
            vecClusterCenter.push_back(clustersInEvent[icluster].getCenter());
            vecClusterFirst.push_back(clustersInEvent[icluster].getFirst());
            vecClusterSize.push_back(clustersInEvent[icluster].getSize());

            if (verbose) {
                std::cout << "  Chamber " << chamber;
                std::cout << " eta " << clustersInEvent[icluster].getEta();
                std::cout << " center " << clustersInEvent[icluster].getCenter();
                std::cout << " size " << clustersInEvent[icluster].getSize();
            }
            // create rechit from cluster on chosen detector:
            rechit = setupGeometry.getChamber(chamber)->createRechit(clustersInEvent[icluster]);
            vecRechitChamber.push_back(chamber);
            vecRechitEta.push_back(clustersInEvent[icluster].getEta());
            vecRechitX.push_back(rechit.getX());
            vecRechitY.push_back(rechit.getY());
            vecRechitErrorX.push_back(rechit.getErrorX());
            vecRechitErrorY.push_back(rechit.getErrorY());
            vecRechitR.push_back(rechit.getR());
            vecRechitPhi.push_back(rechit.getPhi());
            vecRechitClusterSize.push_back(rechit.getClusterSize());

            if (verbose) {
                std::cout << " local (" << rechit.getX() << ",";
                std::cout << rechit.getY() << ") ";
                std::cout << "± (" << rechit.getErrorX() << ",";
                std::cout << rechit.getErrorY() << ")";
                std::cout << std::endl;
            }
        }
        rechitTree.Fill();
    }
    std::cout << std::endl;

    rechitTree.Write();
    rechitFile.Close();
    std::cout << "Output file saved to " << ofile << std::endl;
}
