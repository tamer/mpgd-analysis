#include <cstdio>
#include <iostream>
#include <fstream>
#include <cstdint>
#include <vector>
#include <set>
#include <array>
#include <bitset>
#include <signal.h>
#include <math.h>
#include <sys/stat.h>

#include <chrono>
#include <thread>

#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TH2D.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TF1.h>
#include <TGraphErrors.h>
#include <TFitResult.h>
#include <TFitResultPtr.h>

#include "Digi.h"
#include "Cluster.h"
#include "Rechit.h"
#include "Hit.h"
#include "Track.h"
#include "DetectorGeometry.h"
#include "SetupGeometry.h"

#include "progressbar.h"

bool isInterrupted = false;
void interruptHandler(int dummy) {
    isInterrupted = true;
}

int main (int argc, char** argv) {

    if (argc<3) {
        std::cout << "Usage: Tracks ifile ofile [--geometry geometry_name] [--verbose] [--events n] [-x corrections x] [-y corrections y] [--angles correction angles]" << std::endl;
        return 0;
    }
    std::string ifile = argv[1];
    std::string ofile = argv[2];

    int max_events = -1;
    bool verbose = false;
    std::string geometry;
    for (int iarg=0; iarg<argc; iarg++) {
        std::string arg = argv[iarg];
        if (arg=="--verbose") verbose = true;
        else if (arg=="--events") max_events = atoi(argv[iarg+1]);
        else if (arg=="--geometry") geometry = argv[iarg+1];
    }

    if (max_events > 0) std::cout << "Analyzing " << max_events << " events" << std::endl;
    else std::cout << "Analyzing all events" << std::endl; 

    TFile rechitFile(ifile.c_str(), "READ");     
    TTree *rechitTree = (TTree *) rechitFile.Get("rechitTree");

    TFile trackFile(ofile.c_str(), "RECREATE", "Track file");
    TTree trackTree("trackTree", "trackTree");

   
    // Rechit variables
    int nrechits;
    int orbitNumber, bunchCounter, eventCounter;
    std::vector<int> *vecRechitChamber = new std::vector<int>();
    std::vector<int> *vecRechitEta = new std::vector<int>();
    std::vector<double> *vecRechitX = new std::vector<double>();
    std::vector<double> *vecRechitY = new std::vector<double>();
    std::vector<double> *vecRechitErrorX = new std::vector<double>();
    std::vector<double> *vecRechitErrorY = new std::vector<double>();
    std::vector<double> *vecRechitR = new std::vector<double>();
    std::vector<double> *vecRechitPhi = new std::vector<double>();
    std::vector<double> *vecRechitClusterSize = new std::vector<double>();
    std::vector<int> *vecClusterCenter = new std::vector<int>();
    std::vector<int> *vecDigiStrip = new std::vector<int>();
    std::vector<int> *vecRawChannel = new std::vector<int>();

    // event branches
    rechitTree->SetBranchAddress("orbitNumber", &orbitNumber);
    rechitTree->SetBranchAddress("bunchCounter", &bunchCounter);
    rechitTree->SetBranchAddress("eventCounter", &eventCounter);

    // rechit branches
    rechitTree->SetBranchAddress("rechitChamber", &vecRechitChamber);
    rechitTree->SetBranchAddress("rechitEta", &vecRechitEta);
    rechitTree->SetBranchAddress("rechitX", &vecRechitX);
    rechitTree->SetBranchAddress("rechitY", &vecRechitY);
    rechitTree->SetBranchAddress("rechitErrorX", &vecRechitErrorX);
    rechitTree->SetBranchAddress("rechitErrorY", &vecRechitErrorY);
    rechitTree->SetBranchAddress("rechitR", &vecRechitR);
    rechitTree->SetBranchAddress("rechitPhi", &vecRechitPhi);
    rechitTree->SetBranchAddress("clusterCenter", &vecClusterCenter);
    rechitTree->SetBranchAddress("rechitClusterSize", &vecRechitClusterSize);
    rechitTree->SetBranchAddress("digiStrip", &vecDigiStrip);
    rechitTree->SetBranchAddress("rawChannel", &vecRawChannel);

    /* Track variables 
     * Divided in three groups:
     * - rechit variables
     * - partial track variables and related prophits
     * - full track variables and related prophits
     */

    /** Rechits **/
    std::vector<int> rechitChamber;
    std::vector<double> rechitEta;
    std::vector<double> rechitLocalX;
    std::vector<double> rechitLocalY;
    std::vector<double> rechitR;
    std::vector<double> rechitPhi;
    std::vector<double> rechitErrorX;
    std::vector<double> rechitErrorY;
    std::vector<double> rechitGlobalX;
    std::vector<double> rechitGlobalY;
    std::vector<double> rechitErrorGlobalX;
    std::vector<double> rechitErrorGlobalY;
    std::vector<double> rechitClusterSize;
    std::vector<double> rechitClusterCenter;

    /** Partial tracks **/
    std::vector<int> partialTrackChamber; // chamber excluded from the track
    std::vector<double> partialTrackChi2;
    std::vector<double> partialTrackCovarianceX, partialTrackCovarianceY;
    std::vector<double> partialTrackSlopeX, partialTrackSlopeY;
    std::vector<double> partialTrackInterceptX, partialTrackInterceptY;
    std::vector<double> partialProphitEta;
    std::vector<double> partialProphitGlobalX, partialProphitGlobalY;
    std::vector<double> partialProphitErrorX, partialProphitErrorY;

    /** Tracks built using all tracking detectors **/
    double trackChi2;
    double trackCovarianceX, trackCovarianceY;
    double trackSlopeX, trackSlopeY;
    double trackInterceptX, trackInterceptY;
    std::vector<double> allChi2; // contains chi2 of discarded tracks as well
    std::vector<int> prophitChamber;
    std::vector<double> prophitEta;
    std::vector<double> prophitGlobalX;
    std::vector<double> prophitGlobalY;
    std::vector<double> prophitErrorX;
    std::vector<double> prophitErrorY;
    std::vector<double> prophitLocalX;
    std::vector<double> prophitLocalY;
    std::vector<double> prophitR;
    std::vector<double> prophitPhi;

    // Event branches
    trackTree.Branch("orbitNumber", &orbitNumber);
    trackTree.Branch("bunchCounter", &bunchCounter);
    trackTree.Branch("eventCounter", &eventCounter);
    // Rechit branches propagated from rechit tree with no change
    trackTree.Branch("rechitDigiStrip", vecDigiStrip);
    trackTree.Branch("rechitRawChannel", vecRawChannel);
    // Rechit branches for new variables
    trackTree.Branch("rechitChamber", &rechitChamber);
    trackTree.Branch("rechitEta", &rechitEta);
    trackTree.Branch("rechitLocalX", &rechitLocalX);
    trackTree.Branch("rechitLocalY", &rechitLocalY);
    trackTree.Branch("rechitR", &rechitR);
    trackTree.Branch("rechitPhi", &rechitPhi);
    trackTree.Branch("rechitErrorX", &rechitErrorX);
    trackTree.Branch("rechitErrorY", &rechitErrorY);
    trackTree.Branch("rechitGlobalX", &rechitGlobalX);
    trackTree.Branch("rechitGlobalY", &rechitGlobalY);
    trackTree.Branch("rechitErrorGlobalX", &rechitErrorGlobalX);
    trackTree.Branch("rechitErrorGlobalY", &rechitErrorGlobalY);
    trackTree.Branch("rechitClusterSize", &rechitClusterSize);
    trackTree.Branch("rechitClusterCenter", &rechitClusterCenter);
    // Partial track branches
    trackTree.Branch("partialTrackChamber", &partialTrackChamber);
    trackTree.Branch("partialTrackChi2", &partialTrackChi2);
    trackTree.Branch("partialTrackCovarianceX", &partialTrackCovarianceX);
    trackTree.Branch("partialTrackCovarianceY", &partialTrackCovarianceY);
    trackTree.Branch("partialTrackSlopeX", &partialTrackSlopeX);
    trackTree.Branch("partialTrackSlopeY", &partialTrackSlopeY);
    trackTree.Branch("partialTrackInterceptX", &partialTrackInterceptX);
    trackTree.Branch("partialTrackInterceptY", &partialTrackInterceptY);
    trackTree.Branch("partialProphitEta", &partialProphitEta);
    trackTree.Branch("partialProphitGlobalX", &partialProphitGlobalX);
    trackTree.Branch("partialProphitGlobalY", &partialProphitGlobalY);
    trackTree.Branch("partialProphitErrorX", &partialProphitErrorX);
    trackTree.Branch("partialProphitErrorY", &partialProphitErrorY);
    // Tracks build with all tracking detectors: 
    trackTree.Branch("trackChi2", &trackChi2, "trackChi2/D");
    trackTree.Branch("trackCovarianceX", &trackCovarianceX, "trackCovarianceX/D");
    trackTree.Branch("trackCovarianceY", &trackCovarianceY, "trackCovarianceY/D");
    trackTree.Branch("trackSlopeX", &trackSlopeX, "trackSlopeX/D");
    trackTree.Branch("trackSlopeY", &trackSlopeY, "trackSlopeY/D");
    trackTree.Branch("trackInterceptX", &trackInterceptX, "trackInterceptX/D");
    trackTree.Branch("trackInterceptY", &trackInterceptY, "trackInterceptY/D");
    trackTree.Branch("allChi2", &allChi2);
    trackTree.Branch("prophitChamber", &prophitChamber);
    trackTree.Branch("prophitEta", &prophitEta);
    trackTree.Branch("prophitGlobalX", &prophitGlobalX);
    trackTree.Branch("prophitGlobalY", &prophitGlobalY);
    trackTree.Branch("prophitErrorX", &prophitErrorX);
    trackTree.Branch("prophitErrorY", &prophitErrorY);
    trackTree.Branch("prophitLocalX", &prophitLocalX);
    trackTree.Branch("prophitLocalY", &prophitLocalY);
    trackTree.Branch("prophitR", &prophitR);
    trackTree.Branch("prophitPhi", &prophitPhi);

    /* Define detector geometries */
    SetupGeometry setupGeometry("geometry/setups/"+geometry+".xml");
    const int nTrackers = setupGeometry.trackerChambers.size();

    int chamber;
    double rechitX, rechitY;
    double rechitXError, rechitYError;
    double rechitClusterSizeX, rechitClusterSizeY;
    double prophitX, prophitY;
    double prophitXError, prophitYError;
    Rechit rechit;
    Hit hit;
    Track track;
    // Will hold all possible tracks built with all tracker, choose only one at the end:
    std::vector<Track> trackerTempTracks; 

    int nentries = rechitTree->GetEntries();
    int nentriesGolden = 0, nEntriesWithPartialTracks = 0, nentriesSaved = 0;
    // Support map from chamber ID to number of hits to exclude events with more than one hit per tracker:
    std::map<int, double> hitsPerTrackingChamber;

    std::cout << nentries << " total events" <<  std::endl;
    if (max_events>0) nentries = max_events;
    progressbar bar(nentries);
    signal(SIGINT, interruptHandler);
    for (int nevt=0; (!isInterrupted) && rechitTree->LoadTree(nevt)>=0; ++nevt) {
        if ((max_events>0) && (nevt>max_events)) break;

        if (verbose) std::cout << "Event " << nevt << "/" << nentries << std::endl;
        else bar.update();

        /* Reset support variables */
        for (int chamber: setupGeometry.trackerChambers) {
            hitsPerTrackingChamber[chamber] = 0;
        }
        trackerTempTracks.clear();
        /* Reset branch variables */
        rechitChamber.clear();
        rechitEta.clear();
        rechitLocalX.clear();
        rechitLocalY.clear();
        rechitR.clear();
        rechitPhi.clear();
        rechitErrorX.clear();
        rechitErrorY.clear();
        rechitGlobalX.clear();
        rechitGlobalY.clear();
        rechitErrorGlobalX.clear();
        rechitErrorGlobalY.clear();
        rechitClusterSize.clear();
        rechitClusterCenter.clear();
        partialTrackChamber.clear();
        partialTrackChi2.clear();
        partialTrackCovarianceX.clear();
        partialTrackCovarianceY.clear();
        partialTrackSlopeX.clear();
        partialTrackSlopeY.clear();
        partialTrackInterceptX.clear();
        partialTrackInterceptY.clear();
        partialProphitEta.clear();
        partialProphitGlobalX.clear();
        partialProphitGlobalY.clear();
        partialProphitErrorX.clear();
        partialProphitErrorY.clear();
        allChi2.clear();
        prophitChamber.clear();
        prophitEta.clear();
        prophitGlobalX.clear();
        prophitGlobalY.clear();
        prophitErrorX.clear();
        prophitErrorY.clear();
        prophitLocalX.clear();
        prophitLocalY.clear();
        prophitR.clear();
        prophitPhi.clear();

        rechitTree->GetEntry(nevt);
        nrechits = vecRechitChamber->size();

        /* Building partial tracks
         *
         * @requires at most one rechit per tracking detector
         */
        // Go ahead with partial tracks only if at most one rechit per tracking chamber:
        bool buildPartialTracks = true;
        for (int irechit=0; irechit<nrechits; irechit++) {
            chamber = vecRechitChamber->at(irechit);
            if (setupGeometry.isTracking(chamber)) {
                if (hitsPerTrackingChamber[chamber]>0) buildPartialTracks = false;
                hitsPerTrackingChamber[chamber]++;
            }
        }
        if (verbose) {
            for (auto pair:hitsPerTrackingChamber) {
                std::cout << "  " << pair.second << " rechits in chamber " << pair.first << std::endl;
            }
        }

        if (buildPartialTracks) {

            if (verbose) {
                std::cout << "  #### Extrapolation on tracker ####" << std::endl;
            }
            nEntriesWithPartialTracks++;

            for (int excludedChamber : setupGeometry.trackerChambers) {
                track.clear();
                for (int irechit=0; irechit<nrechits; irechit++) {
                    chamber = vecRechitChamber->at(irechit);
                    if (!setupGeometry.isTracking(chamber)) continue;
                    rechit = Rechit(
                            chamber,
                            vecRechitX->at(irechit), vecRechitY->at(irechit),
                            vecRechitErrorX->at(irechit), vecRechitErrorY->at(irechit),
                            vecRechitClusterSize->at(irechit)
                            );
                    //std::cout << "    chamber " << rechit.getChamber() << " angle " << setupGeometry.getChamber(chamber)->getTheta() << std::endl;
                    //std::cout << "                 \t" << "x" << "\t" << "y" << std::endl;
                    //std::cout << "    global before\t" << rechit.getGlobalX() << "\t" << rechit.getGlobalY() << std::endl;

                    // Apply global geometry:
                    setupGeometry.getChamber(chamber)->mapRechit(&rechit);

                    //std::cout << "            local\t" << rechit.getX() << "\t" << rechit.getY() << std::endl;
                    //std::cout << "     global after\t" << rechit.getGlobalX() << "\t" << rechit.getGlobalY() << std::endl;
                    if (chamber!=excludedChamber) {
                        track.addRechit(rechit);
                    } else { // Add rechit of excluded chamber to tree
                        rechitChamber.push_back(chamber);
                        rechitEta.push_back(vecRechitEta->at(irechit));
                        rechitLocalX.push_back(rechit.getX());
                        rechitLocalY.push_back(rechit.getY());
                        rechitErrorX.push_back(rechit.getErrorX());
                        rechitErrorY.push_back(rechit.getErrorY());
                        rechitR.push_back(rechit.getR());
                        rechitPhi.push_back(rechit.getPhi());
                        rechitGlobalX.push_back(rechit.getGlobalX());
                        rechitGlobalY.push_back(rechit.getGlobalY());
                        rechitErrorGlobalX.push_back(rechit.getErrorGlobalX());
                        rechitErrorGlobalY.push_back(rechit.getErrorGlobalY());
                        rechitClusterCenter.push_back(vecClusterCenter->at(irechit));
                        rechitClusterSize.push_back(vecRechitClusterSize->at(irechit));
                    }
                }
                // Fit and save track:
                track.fit();
                partialTrackChamber.push_back(excludedChamber);
                partialTrackChi2.push_back(track.getChi2());
                partialTrackCovarianceX.push_back(track.getCovarianceX());
                partialTrackCovarianceY.push_back(track.getCovarianceY());
                partialTrackSlopeX.push_back(track.getSlopeX());
                partialTrackSlopeY.push_back(track.getSlopeY());
                partialTrackInterceptX.push_back(track.getInterceptX());
                partialTrackInterceptY.push_back(track.getInterceptY());

                // Propagate to chamber under test:
                hit = track.propagate(setupGeometry.getChamber(excludedChamber));
                partialProphitGlobalX.push_back(hit.getGlobalX());
                partialProphitGlobalY.push_back(hit.getGlobalY());
                partialProphitErrorX.push_back(hit.getErrorX());
                partialProphitErrorY.push_back(hit.getErrorY());

                if (verbose) {
                    std::cout << "  Chamber " << excludedChamber << std::endl;
                    std::cout << "    " << "track slope (" << track.getSlopeX() << "," << track.getSlopeY() << ")";
                    std::cout << " " << "intercept (" << track.getInterceptX() << "," << track.getInterceptY() << ")";
                    std::cout << " " << "chi2 " << track.getChi2Reduced();
                    std::cout << std::endl;

                    if (rechitLocalX.size()>0) {
                        std::cout << "    " << "rechit (" << rechitGlobalX.back();
                        std::cout << "," << rechitGlobalY.back() << ")";
                        std::cout << " ± (" << rechitErrorGlobalX.back();
                        std::cout << "," << rechitErrorGlobalY.back() << ")";
                        std::cout << "  " << "prophit (" << partialProphitGlobalX.back();
                        std::cout << "," << partialProphitGlobalY.back() << ")";
                        std::cout << std::endl;
                    }
                    std::cout << "    Built using hits:" << std::endl;
                    for (int ip=0; ip<track.getRechitPositions(0).size(); ip++) {
                        std::cout << "    x " << track.getRechitPositions(0).at(ip);
                        std::cout << " y " << track.getRechitPositions(1).at(ip);
                        std::cout << " z " << track.getRechitZ().at(ip);
                        std::cout << std::endl;
                    }
                }
            }
        } else {
            if (verbose) {
                std::cout << "  More than one hit in a tracking detector, skipping partial tracks..." << std::endl; 
            }
        }

        if (verbose) {
            std::cout << "  #### Extrapolation on non-tracking detectors ####" << std::endl;
        }

        /* Building full tracks
         *
         * Create all possible tracks with all tracking detectors,
         * then keep only the track with the lowest chi squared 
         */
        trackerTempTracks.clear();
        allChi2.clear();

        // Create array with tracking detectors only:
        std::vector<int> trackingChambers;
        for (int irechit=0; irechit<nrechits; irechit++) {
            chamber = vecRechitChamber->at(irechit);
            if (setupGeometry.isTracking(chamber)) {
                trackingChambers.push_back(chamber);
            }
        }
        // Create unique array of chambers:
        std::set<int> chambersUniqueSet(trackingChambers.begin(), trackingChambers.end());
        std::vector<int> chambersUnique(chambersUniqueSet.begin(), chambersUniqueSet.end());
        int nTrackersInEvent = chambersUnique.size();
        if (verbose) {
            std::cout << "  There are " << nTrackersInEvent << " trackers in the event: [";
            for (auto c:chambersUnique) std::cout << " " << c;
            std::cout << " ]" << std::endl;
        }
        if (nTrackersInEvent < 3) {
            if (verbose) {
                std::cout << "  Not enough trackers, skipping event..." << std::endl;
            }
            continue;
        }
        // Divide the rechit indices in one vector per chamber:
        std::vector<std::vector<int>> rechitIndicesPerChamber(nTrackersInEvent);
        for (int irechit=0; irechit<nrechits; irechit++) {
            chamber = vecRechitChamber->at(irechit);
            if (!setupGeometry.isTracking(chamber)) continue;
            int chamberIndex = std::find(chambersUnique.begin(), chambersUnique.end(), chamber) - chambersUnique.begin();
            rechitIndicesPerChamber[chamberIndex].push_back(irechit);
        }
        if (verbose) {
            for (int i=0; i<rechitIndicesPerChamber.size(); i++) {
                std::cout << "    Chamber " << chambersUnique[i] << ": ";
                for (auto rechitIndex:rechitIndicesPerChamber[i]) std::cout << rechitIndex << " ";
                std::cout << std::endl;
            }
        }
        // Create an array with an iterator for each chamber:
        std::vector<std::vector<int>::iterator> iterators;
        for (auto it=rechitIndicesPerChamber.begin(); it!=rechitIndicesPerChamber.end(); it++) {
            iterators.push_back(it->begin());
        }

        // Skip event if too many spurious hits:
        int nPossibleTracks = 1;
        for (auto el:rechitIndicesPerChamber) nPossibleTracks *= el.size();
        if (verbose) {
            std::cout << "    There are " << nPossibleTracks << " possible tracks in the event..." << std::endl;
        }
        if (nPossibleTracks > 50) continue;

        /* Create all possible rechit combinations per tracker
         * using "odometer" method:
         * https://stackoverflow.com/a/1703575
         */
        while (iterators[0] != rechitIndicesPerChamber[0].end()) {
            // build the track with current rechit combination:
            //std::this_thread::sleep_for(std::chrono::milliseconds(50));
            Track testTrack;
            for (auto it:iterators) {
                int rechitIndex = *it;
                chamber = vecRechitChamber->at(rechitIndex);
                rechit = Rechit(
                        chamber,
                        vecRechitX->at(rechitIndex), vecRechitY->at(rechitIndex),
                        vecRechitErrorX->at(rechitIndex), vecRechitErrorY->at(rechitIndex),
                        vecRechitClusterSize->at(rechitIndex)
                        );
                setupGeometry.getChamber(chamber)->mapRechit(&rechit); // apply local geometry
                testTrack.addRechit(rechit);
            }
            // Build track and append it to the list:
            testTrack.fit();
            if (!testTrack.isValid()) {
                trackChi2 = -1.;
            } else {
                trackChi2 = testTrack.getChi2Reduced();
            }
            trackerTempTracks.push_back(testTrack);
            allChi2.push_back(trackChi2);
            if (verbose) {
                std::cout << "    Built track with rechit IDs: ";
                for (auto it:iterators) std::cout << *it << " ";
                std::cout << " and chi2 " << trackChi2 << std::endl;
            }

            iterators[nTrackersInEvent-1]++; // always scan the least significant vector
            for (int iChamber=nTrackersInEvent-1; (iChamber>0) && (iterators[iChamber]==rechitIndicesPerChamber[iChamber].end()); iChamber--) {
                // if a vector arrived at the end, restart from the beginning
                // and increment the vector one level higher:
                iterators[iChamber] = rechitIndicesPerChamber[iChamber].begin();
                iterators[iChamber-1]++;
            }
        }
        int bestTrackIndex = 0;
        double bestTrackChi2 = allChi2.at(0);
        double presentTrackChi2;
        for (int i=0; i<trackerTempTracks.size(); i++) {
            //presentTrackChi2 = trackerTempTracks.at(i).getChi2ReducedX()+trackerTempTracks.at(i).getChi2ReducedY();
            presentTrackChi2 = allChi2.at(i);
            if (presentTrackChi2<=bestTrackChi2) {
                bestTrackIndex = i;
                bestTrackChi2 = presentTrackChi2;
            }
        }
        track = trackerTempTracks.at(bestTrackIndex);
        allChi2.erase(allChi2.begin() + bestTrackIndex);
        if (verbose) {
            std::cout << "    Found best track at index " << bestTrackIndex;
            std::cout << " with chi2x " << track.getChi2X();
            std::cout << " and chi2y " << track.getChi2Y() << ". ";
            std::cout << "Slope x " << track.getSlopeX() << ", intercept x " << track.getInterceptX() << ", ";
            std::cout << "slope y " << track.getSlopeY() << ", intercept y " << track.getInterceptY();
            std::cout << std::endl;
        }

        if (!track.isValid()) {
            trackChi2 = -1.;
        } else {
            trackChi2 = track.getChi2Reduced();
            trackCovarianceX = track.getCovarianceX();
            trackCovarianceY = track.getCovarianceY();
            trackSlopeX = track.getSlopeX();
            trackSlopeY = track.getSlopeY();
            trackInterceptX = track.getInterceptX();
            trackInterceptY = track.getInterceptY();

            // extrapolate track on large detectors
            for (const auto &detectorPair : setupGeometry.detectorMap) {
                chamber = detectorPair.first;
                if (setupGeometry.isTracking(chamber)) continue; // only propagate to non-tracking detectors
                hit = track.propagate(setupGeometry.getChamber(chamber));
                prophitChamber.push_back(chamber);
                prophitEta.push_back(hit.getEta());
                prophitGlobalX.push_back(hit.getGlobalX());
                prophitGlobalY.push_back(hit.getGlobalY());
                prophitErrorX.push_back(hit.getErrorX());
                prophitErrorY.push_back(hit.getErrorY());
                prophitLocalX.push_back(hit.getLocalX());
                prophitLocalY.push_back(hit.getLocalY());
                prophitR.push_back(hit.getLocalR());
                prophitPhi.push_back(hit.getLocalPhi());

                if (verbose) {
                    std::cout << "  Chamber " << chamber << std::endl;
                    std::cout << "    " << "track slope (" << track.getSlopeX() << "," << track.getSlopeY() << ")";
                    std::cout << " " << "intercept (" << track.getInterceptX() << "," << track.getInterceptY() << ")";
                    std::cout << std::endl;
                    std::cout << "    " << "prophit " << "eta=" << hit.getEta() << ", ";
                    std::cout << "global carthesian (" << hit.getGlobalX() << "," << hit.getGlobalY() << "), ";
                    std::cout << "local carthesian (" << hit.getLocalX() << "," << hit.getLocalY() << "), ";
                    std::cout << "polar R=" << hit.getLocalR() << ", phi=" << hit.getLocalPhi();
                    std::cout << std::endl;
                }
            }
        }

        // save all 1D rechits local coordinates
        for (int irechit=0; irechit<vecRechitChamber->size(); irechit++) {
            chamber = vecRechitChamber->at(irechit);
            if (setupGeometry.isTracking(chamber)) continue; // only save tracks from non-tracking detectors
            if (verbose) std::cout << "  Chamber " << chamber << std::endl;
            hit = Hit::fromLocal(setupGeometry.getChamber(chamber),
                    vecRechitX->at(irechit), vecRechitY->at(irechit),
                    vecRechitErrorX->at(irechit), vecRechitErrorY->at(irechit), 0.
                    );
            rechitChamber.push_back(chamber);
            rechitEta.push_back(vecRechitEta->at(irechit));
            rechitLocalX.push_back(hit.getLocalX());
            rechitLocalY.push_back(hit.getLocalY());
            rechitR.push_back(hit.getLocalR());
            rechitPhi.push_back(hit.getLocalPhi());
            rechitGlobalX.push_back(hit.getGlobalX());
            rechitGlobalY.push_back(hit.getGlobalY());
            rechitErrorX.push_back(hit.getErrorX());
            rechitErrorY.push_back(hit.getErrorY());
            rechitClusterSize.push_back(vecRechitClusterSize->at(irechit));
            rechitClusterCenter.push_back(vecClusterCenter->at(irechit));
            if (verbose) {
                std::cout << "    " << "rechit  " << "eta=" << vecRechitEta->at(irechit) << ", ";
                std::cout << "global carthesian (" << rechitGlobalX.back() << "," << rechitGlobalY.back() << "), ";
                std::cout << "local carthesian (" << hit.getLocalX() << "," << hit.getLocalY() << "), ";
                std::cout << "local polar R=" << hit.getLocalR() << ", phi=" << hit.getLocalPhi();
                std::cout << std::endl;
            }
        }

        trackTree.Fill();
        nentriesSaved++;
    }
    std::cout << std::endl;
    std::cout << "Entries with partial tracks: " << nEntriesWithPartialTracks << std::endl;
    std::cout << "Saved entries: " << nentriesSaved << std::endl;

    trackTree.Write();
    trackFile.Close();
    std::cout << "Output files written to " << ofile << std::endl;
}
